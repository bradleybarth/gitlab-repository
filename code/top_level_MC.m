%%  PARSE and PROCESS MEA DATA
%   Save the events as a data structure
parse_to_save_EMGSingleThreshold(2705)
parse_to_save_EMGSingleThreshold(3402)
parse_to_save_EMGSingleThreshold(5603)
parse_to_save_EMGSingleThreshold(6002)
parse_to_save_EMGSingleThreshold(6497)
parse_to_save_EMGSingleThreshold(6498)
%%

%%  LOAD, COMPILE and SAVE ALL DATA
loadpath = 'C:\Users\Bradley Barth\Box\Home Folder bbb16\Sync\School\grill_lab\projects\multi-electrode-array\gitlab-repository\processed-data\';
load(fullfile(loadpath,'2705_EMG.mat'))
MC_2705 = MC;
load(fullfile(loadpath,'3402_EMG.mat'))
MC_3402 = MC;
load(fullfile(loadpath,'5603_EMG.mat'))
MC_5603 = MC;
load(fullfile(loadpath,'6002_EMG.mat'))
MC_6002 = MC;
load(fullfile(loadpath,'6497_EMG.mat'))
MC_6497 = MC;
load(fullfile(loadpath,'6498_EMG.mat'))
MC_6498 = MC;

MC = [MC_2705 MC_3402 MC_5603 MC_6002 MC_6497 MC_6498];
savepath = 'C:\Users\Bradley Barth\Box\Home Folder bbb16\Sync\School\grill_lab\projects\multi-electrode-array\gitlab-repository\processed-data\';
save(fullfile(savepath,'MC_2019_12_03.mat'),'MC')
%%

%%  PARSE MC DATA and WRITE TO CSV TABLE
fileID = fopen(fullfile(savepath,'mc-data.txt'),'w');
formatSpec = '%i,%s,%i,%0.3f,%0.3f,%0.3f,%0.3f,%0.3f\n';
for i = 1:length(MC)
    b = logical(MC(i).data.time < 30*60);
    s = logical((MC(i).data.time > 30*60) .* (MC(i).data.time < 45*60));
    baseRate = sum(b)/0.5; %units are events per hour
    SNSRate = sum(s)/0.25; %units are events per hour
    baseWidth = mean(MC(i).data.width(b));
    SNSWidth = mean(MC(i).data.width(s));
    fprintf(fileID,formatSpec,MC(i).rat,MC(i).drug,MC(i).day,MC(i).threshold,...
        baseRate,SNSRate,baseWidth,SNSWidth);
end
fclose(fileID);
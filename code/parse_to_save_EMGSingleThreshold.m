function parse_to_save_EMGSingleThreshold(ID)
% master executable to run all MATLAB processes
% ID refers to specimen number (Rat_ID)
close all;

str = {'S','L','C'};
if strcmp(ID,'test') || strcmp(ID,'test2')
    str = {'S'};
end

name = num2str(ID);
SAVELOC = 'C:\Users\Bradley Barth\Box\Home Folder bbb16\Sync\School\grill_lab\projects\multi-electrode-array\gitlab-repository\processed-data';
SAVEFILE = ['C:\Users\Bradley Barth\Box\Home Folder bbb16\Sync\School\grill_lab\projects\multi-electrode-array\gitlab-repository\processed-data\',name,'_EMG.mat'];


MC.rat = [];
MC.day = [];
MC.date = [];
MC.recording = {};
MC.drug = {};
MC.condition = {};
MC.time = [];
MC.width = [];
MC.amp = [];
MC.auc = [];
MC.offsetPass = [];

for i = 1:length(str)
    loc = ['F:/Gen2 MEA/Data/RatID_',num2str(ID),'/ID.',num2str(ID),' 1_9/'];

    s = {[loc,'standard_d1/'] [loc,'standard_d2/'] [loc,'standard_d3/'] ...
        [loc,'standard_d4/'] [loc,'standard_d5/'] [loc,'standard_d6/'] ...
        [loc,'standard_d7/'] [loc,'standard_d8/'] [loc,'standard_d9/']};
    l = {[loc,'lidocaineSNS_1/'] [loc,'lidocaineSNS_2/'] [loc,'lidocaineSNS_3/'] ...
        [loc,'lidocaineSham_1/'] [loc,'lidocaineSham_2/'] [loc,'lidocaineSham_3/']};
    c = {[loc,'atropine_1/'] [loc,'atropine_2/'] [loc,'atropine_3/'] ...
        [loc,'hexamethonium_1/'] [loc,'hexamethonium_2/'] [loc,'hexamethonium_3/']};
    
    Ldrugs = {'Lidocaine SNS','Lidocaine SNS','Lidocaine SNS',...
        'Lidocaine Sham','Lidocaine Sham','Lidocaine Sham'};
    Cdrugs = {'Atropine SNS','Atropine SNS','Atropine SNS',...
        'Hexamethonium SNS','Hexamethonium SNS','Hexamethonium SNS'};
    
    if ID == 3402
        s = {[loc,'standard_d1/'] [loc,'standard_d2/'] [loc,'standard_d3/'] ...
            [loc,'standard_d4/'] [loc,'standard_d5/'] [loc,'standard_d6/']};
        l = {[loc,'lidocaineSNS_1/'] [loc,'lidocaineSNS_2/'] ...
            [loc,'lidocaineSham_1/']};
        c = {[loc,'atropine_1/'] [loc,'atropine_2/'] ...
            [loc,'hexamethonium_1/'] [loc,'hexamethonium_2/']};
        
        Ldrugs = {'Lidocaine SNS','Lidocaine SNS',...
            'Lidocaine Sham'};
        Cdrugs = {'Atropine SNS','Atropine SNS',...
            'Hexamethonium SNS','Hexamethonium SNS'};
        
    end
    
    if ID == 5603
        s = {[loc,'standard_d1/'] [loc,'standard_d2/'] [loc,'standard_d3/'] ...
            [loc,'standard_d4/'] [loc,'standard_d5/'] [loc,'standard_d6/']...
            [loc,'standard_d7/']};
        l = {[loc,'lidocaineSNS_1/'] [loc,'lidocaineSNS_2/'] [loc,'lidocaineSNS_3/'] ...
            [loc,'lidocaineSham_1/'] [loc,'lidocaineSham_2/'] [loc,'lidocaineSham_3/']};
        c = {[loc,'atropine_1/'] [loc,'atropine_2/'] [loc,'atropine_3/']...
            [loc,'hexamethonium_1/']};
        
        Ldrugs = {'Lidocaine SNS','Lidocaine SNS','Lidocaine SNS',...
            'Lidocaine Sham','Lidocaine Sham','Lidocaine Sham'};
        Cdrugs = {'Atropine SNS','Atropine SNS','Atropine SNS',...
            'Hexamethonium SNS'};
        
    end
    
    if ID == 6497
        s = {[loc,'standard_d1/'] [loc,'standard_d2/'] [loc,'standard_d3/'] ...
            [loc,'standard_d4/']};
        l = {[loc,'lidocaineSNS_1/'] [loc,'lidocaineSham_1/']};
        c = {[loc,'atropine_1/'] [loc,'hexamethonium_1/']};
        
        Ldrugs = {'Lidocaine SNS','Lidocaine Sham'};
        Cdrugs = {'Atropine SNS','Hexamethonium SNS'};
        
    end
    
    if ID == 6498
        s = {[loc,'standard_d1/'] [loc,'standard_d2/'] [loc,'standard_d3/'] ...
            [loc,'standard_d4/'] [loc,'standard_d5/'] [loc,'standard_d6/']...
            [loc,'standard_d7/'] [loc,'standard_d8/'] [loc,'standard_d9/']};
        l = {[loc,'lidocaineSNS_1/'] [loc,'lidocaineSNS_2/'] [loc,'lidocaineSNS_3/'] ...
            [loc,'lidocaineSham_1/'] [loc,'lidocaineSham_2/'] [loc,'lidocaineSham_3/']};
        c = {[loc,'atropine_1/'] [loc,'atropine_2/'] [loc,'atropine_3/']...
            [loc,'hexamethonium_1/'] [loc,'hexamethonium_2/']};
        
        Ldrugs = {'Lidocaine SNS','Lidocaine SNS','Lidocaine SNS',...
            'Lidocaine Sham','Lidocaine Sham','Lidocaine Sham'};
        Cdrugs = {'Atropine SNS','Atropine SNS','Atropine SNS',...
            'Hexamethonium SNS' 'Hexamethonium SNS'};
        
    end
    
    if strcmp(ID,'test')
        s = {['F:\Gen2 MEA\Data\RatID_3402\ID.3402 1_9\standard_d1']};
        l = {};
        c = {};
    elseif strcmp(ID,'test2')
        s = {['F:\Gen2 MEA\Data\RatID_3402\ID.3402 1_9\standard_d1']};
        l = {};
        c = {};
    end

    LOAD_FOLDER = [];
    lbl = {'S','L','C'};
    grp = {s, l, c};
    for j = 1:3
        if max(lbl{j} == str{i})
            LOAD_FOLDER = [LOAD_FOLDER grp{j}];
        end
    end

    BASELINE_TIME = 30*60; % seconds
    CONDITION_TIME = 15*60; % seconds
    % After baseline, 15 min stim on, followd by 15 min off. On-off
    % then repeated a second time. 

    
    day = 1;
    for f = 1:numel(LOAD_FOLDER)
        
        rawData = {};
        rawWindowTimes = {};
        
        begin = datetime('now');
        
        thisFolder = LOAD_FOLDER{f};
        folderContents = dir(thisFolder);
        
        K = min([62 length(folderContents)]);
        
        LFP = [];
        LFPtime = [];
        
        for k = 1:K
            filename = folderContents(k).name;

            % skip undesired files
            if ~contains(filename, '.rhd'), continue, end

            % load data
            try
                [loadedData, time, fs] = ...
                  read_Intan_RHD2000_file_inline(filename, thisFolder);
            catch
                disp('Unable to load RHD file:')
                disp(filename)
                continue
            end

            % subtract common reference
            thisData = loadedData;
            
            chan = [11 12 13 19 20 21];
            for ch=1:length(chan)   %1:32
                dec(ch,:)=decimate(thisData(chan(ch),:),ceil(fs/400));
            end
            dectime=downsample(time,ceil(fs/400));
            LFPtime=[LFPtime dectime];
            LFP=[LFP dec];
            
            dateRec = str2double(filename(12:17));
            
            clear data time dec dectime
        end
        time = LFPtime;
        Fs = round(1/(LFPtime(2)-LFPtime(1)));
%         [b, a] = butter(4, 500/(Fs/2), 'low');
%         filtLFP = filtfilt(b, a, LFP);
%         [b, a] = butter(4, 10/(Fs/2), 'high');
        filtLFP = LFP;

%         DD = zeros(4,length(LFPtime));
%         group = [25 23 21; 13 11 9; 26 24 22; 12 10 8];
%         for d = 1:4
%             DD(d,:) = (filtLFP(group(d,1),:) + filtLFP(group(d,3),:) - ...
%                 2*filtLFP(group(d,2),:))/3;
%         end
        
        lngtdnl1 = filtLFP(2,:) - mean(filtLFP([1 3],:));
        lngtdnl2 = filtLFP(5,:) - mean(filtLFP([4 6],:));
        EMG = mean([abs(lngtdnl1); abs(lngtdnl2)]);

        clear amp width peaktime auc start stop
        [amp, width, peaktime, auc, start, stop] = contractDetectSingleThreshold(EMG,Fs,1.0);
        %base = logical(peaktime{d}<BASELINE_TIME);
        %SNS = logical((peaktime{d}>BASELINE_TIME).*(peaktime{d}<(BASELINE_TIME+CONDITION_TIME)));
        %post = logical((peaktime{d}<(BASELINE_TIME+2*CONDITION_TIME)).*(peaktime{d}>(BASELINE_TIME+CONDITION_TIME)));

%         edge1 = 30*60*Fs;
%         edge2 = 45*60*Fs;
% 
%         for ele = 1:4
%             DD(ele,1:edge1) = DD(ele,1:edge1)-mean(DD(ele,1:edge1));
%             DD(ele,edge1:edge2) = DD(ele,edge1:edge2)-mean(DD(ele,edge1:edge2));
%             DD(ele,edge2:end) = DD(ele,edge2:end)-mean(DD(ele,edge2:end));
%         end
% 
%         EMG = movmean(mean(abs(DD-movmean(DD,60*Fs)),1),10*Fs);
%         
%         plot(LFPtime,EMG)
%         hold on
        

        ddEMG =diff(diff(EMG));
        points.X = EMG(ddEMG<0);
        points.T = time(ddEMG<0);
        points.mean = mean(EMG);
        points.std = std(EMG);
        points.rms = rms(EMG);
        
        x = points.X(logical((points.T > 15*60).*(points.T < 30*60)));
        y = points.X(logical((points.T > 30*60).*(points.T < 45*60)));
        
        if exp(abs(log(mean(x)/mean(y)))) > 3
            pass = 0;
        else
            pass = 1;
        end

        for n = 1:length(peaktime)
            if peaktime(n) < BASELINE_TIME, condish = {'baseline'};
            elseif peaktime(n) < BASELINE_TIME+CONDITION_TIME, condish = {'SNS'};
            elseif peaktime(n) < BASELINE_TIME+2*CONDITION_TIME, condish = {'post SNS'};
            elseif peaktime(n) < BASELINE_TIME+3*CONDITION_TIME, condish = {'SNS'};
            else,condish = {'post SNS'};
            end
            MC.condition = [MC.condition; condish{1}];
            MC.time = [MC.time; peaktime(n)];
            MC.width = [MC.width; width(n)];
            MC.amp = [MC.amp; amp(n)];
            MC.rat = [MC.rat; ID];
            MC.offsetPass = [MC.offsetPass; pass];

            if strcmp(str{i},'S'), drug = 'Standard';
            elseif strcmp(str{i},'L'), drug = Ldrugs{day};
            elseif strcmp(str{i},'C'), drug = Cdrugs{day};
            end
            MC.drug = [MC.drug; drug];
            MC.day = [MC.day; day];
            MC.date = [MC.date; dateRec];
            MC.auc = [MC.auc; auc(n)];
            uniqueRecID = [num2str(ID),'_',drug,'_',num2str(day)];
            MC.recording = [MC.recording; uniqueRecID];
            
%             plot([start(n) stop(n)],[0 0],'r-','LineWidth',2)
            
        end
        q = quantile(width,3);
        disp([min(width) q max(width)])
        
        strname = [datestr(now,'yyyy.mm.dd'),'.',num2str(ID),'.',str{i},'.',num2str(day),'points.mat'];
        save(fullfile(SAVELOC,'\points',strname),'points');
        
        day = day + 1;
    end
    
    disp('Started at:')
    disp(begin)
    disp('Finished at:')
    disp(datetime('now'))
    %clear rate int intensity peaktime width amp base SNS post
end

save(SAVEFILE, 'MC')
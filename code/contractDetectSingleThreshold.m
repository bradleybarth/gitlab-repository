function [amp, width, peaktime, auc, start, stop] = contractDetectSingleThreshold(EMG,Fs,wide)

%wide is the minimum width of a contraction

N = length(EMG);
time = (1:N)/Fs;

th = mean(EMG)+2*std(EMG);
filename = 'F:\Gen2 MEA\old_style_MC-threshold.txt';
fid = fopen(filename,'a'); 
fwrite(fid,[num2str(round(th,2)),'\n']);
fclose(fid);

bin = nan(size(EMG));
bin(EMG<th) = 0;
bin(EMG>=th) = 1;
% bin(1:5) = 0; bin(end-5:end) = 0;

difbin = diff(bin);
% onWidth = 5.5; onInd = onWidth * Fs;
% offWidth = 3; offInd = offWidth * Fs;


l = find(difbin>=1);
p = find(difbin<=-1);
C = min([length(l) length(p)]);

env = movmean(EMG,300*Fs);

wLimit = wide*Fs;

i = 1;
while i <= C
    if p(i) - l(i) <= wLimit
        bin(l(i):p(i)) = zeros(size(l(i):p(i)));
    end 
    i = i + 1;
end

clear difbin

difbin = diff(bin);

startIdx = find(difbin>=1);
stopIdx = find(difbin<=-1);

if isempty(stopIdx) || isempty(startIdx)
    amp = nan; width = nan; peaktime = nan;
    auc = nan; start = nan; stop = nan;
    return
end

if stopIdx(1) < startIdx(1)
    stopIdx = stopIdx(2:end);
end
    
C = min([length(startIdx) length(stopIdx)]);
start = time(startIdx(1:C));
stop = time(stopIdx(1:C));

temp = stop-start;
durGreat = logical((temp > wide).*(temp<10));
width = temp(durGreat);

C = sum(durGreat);

peaktime = nan(1,C);
auc = nan(1,C);
amp = nan(1,C);
for i = 1:C
    [~,I] = max(EMG(startIdx(i):stopIdx(i)));
    if isempty(I), continue, end
    peaktime(i) = time(I+startIdx(i));
    auc(i) = sum(EMG(startIdx(i):stopIdx(i)));
    amp(i) = auc(i)/width(i);
end
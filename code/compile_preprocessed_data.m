%%  LOAD, COMPILE and SAVE ALL DATA
loadpath = 'C:\Users\Bradley Barth\Box\Home Folder bbb16\Sync\School\grill_lab\projects\multi-electrode-array\gitlab-repository\processed-data\';
load(fullfile(loadpath,'2705_EMG.mat'))
MC_2705 = MC;
load(fullfile(loadpath,'3402_EMG.mat'))
MC_3402 = MC;
load(fullfile(loadpath,'5603_EMG.mat'))
MC_5603 = MC;
load(fullfile(loadpath,'6002_EMG.mat'))
MC_6002 = MC;
load(fullfile(loadpath,'6497_EMG.mat'))
MC_6497 = MC;
load(fullfile(loadpath,'6498_EMG.mat'))
MC_6498 = MC;

f = fieldnames(MC_2705);
for i = 1:length(f)
    MC.(f{i}) = [MC_2705.(f{i}); MC_3402.(f{i}); MC_5603.(f{i}); MC_6002.(f{i}); MC_6498.(f{i}); MC_6497.(f{i})];
end

strname = [datestr(now,'yyyy.mm.dd'),'.MC.mat'];
save(fullfile(loadpath,strname),'MC');

strname = [datestr(now,'yyyy.mm.dd'),'.MC_compiled.txt'];
fileID = fopen(fullfile(loadpath,strname),'w');
formatSpec = '%i,%i,%s,%i,%i\n';

recs = unique(MC.recording);
for r = 1:length(recs)
    theseMCs = find(strcmp(MC.recording,recs{r}));
    ex = theseMCs(1);
    switch MC.drug{ex}
        case 'Standard'
            treatment = {'C','S'};
            edges = [5 20; 30 45]*60;
        case 'Lidocaine SNS'
            treatment = {'C','L + S'};
            edges = [5 20; 30 45]*60;            
        case 'Lidocaine Sham'
            treatment = {'L'};
            edges = [10 25]*60;
        otherwise
            continue
    end
    tableRows = zeros(length(treatment),1);
    for t = 1:length(theseMCs)
        if MC.time(theseMCs(t)) > edges(1,1) && ...
                MC.time(theseMCs(t)) < edges(1,2)
            tableRows(1) = tableRows(1) + 1;
        elseif length(treatment) == 2 && ...
                MC.time(theseMCs(t)) > edges(2,1) && ...
                MC.time(theseMCs(t)) < edges(2,2)
            tableRows(2) = tableRows(2) + 1;
        end
    end
    if treatment{1} == 'C' && tableRows(1) == 0
        keep = 0;
    else
        keep = 1;
    end
    for i = 1:length(tableRows)
        fprintf(fileID,formatSpec,MC.rat(ex),MC.date(ex),treatment{i},tableRows(i),MC.offsetPass(ex)&&keep);
    end
end
fclose(fileID);
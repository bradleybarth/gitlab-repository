loadFolder = 'C:\Users\Bradley Barth\Box\Home Folder bbb16\Sync\School\grill_lab\projects\multi-electrode-array\gitlab-repository\processed-data\points';
folderContents = dir(loadFolder);
X = []; T = [];
for k = 1:length(folderContents)
    clear x y
    if ~contains(folderContents(k).name,'points.mat')
        continue
    end
    p = load(fullfile(folderContents(k).folder,folderContents(k).name));
  
    x = p.points.X(logical((p.points.T > 15*60).*(p.points.T < 30*60)));
    y = p.points.X(logical((p.points.T > 30*60).*(p.points.T < 45*60)));
    [h,pval] = ttest2(x(x<quantile(x,0.1)),y(y<quantile(y,0.1)));
    
    scatter(p.points.T,p.points.X,'k.')
    hold on
    for i = 0:3
        plot([0 3600],p.points.mean*[1 1]+i*p.points.std*[1 1],'r-','LineWidth',4-i)
    end
    plot([0 3600],p.points.rms*[1 1],'b-','LineWidth',2); hold off
    a = ylim;
    yloc = 0.95*a(2);
    
    if exp(abs(log(mean(x)/mean(y)))) > 3
        pass = 0;
    else
        pass = 1;
    end
    
    s1 = sprintf('%i  %i  %i\n%0.5f\n%i',h,round(mean(x),-1),round(mean(y),-1),log10(pval),pass);
    text(250,yloc,s1)
    pause(0.5)
    
    X = [X p.points.X];
    T = [T p.points.T];
end
th = 200;
scatter(T,X,'k.')
hold on; plot([0 3600],th*[1 1],'r-','LineWidth',2); hold off
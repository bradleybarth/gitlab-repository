load('C:\Users\Bradley Barth\Box\Home Folder bbb16\Sync\School\grill_lab\projects\multi-electrode-array\data\processed-data\in-vivo-MEA.mat')
savepath = 'C:\Users\Bradley Barth\Box\Home Folder bbb16\Sync\School\grill_lab\projects\multi-electrode-array\data\processed-data\';

fileID = fopen(fullfile(savepath,'mc-data.txt'),'w');
fprintf(fileID,'%s,%s,%s,%s,%s,%s,%s,%s\n','Subject','Drug',...
    'Replicate','Site','Threshold','Stimulation','Duration','Interval');
formatSpec = '%s,%s,%i,%i,%i,%i,%.2f,%.2f\n';

for i = 1:length(MCdata)
    for j = 1:length(MCdata{i}.dur)
        fprintf(fileID,formatSpec,num2str(MCdata{i}.subject),MCdata{i}.condition,...
            MCdata{i}.replicate,MCdata{i}.site,MCdata{i}.threshold,...
            MCdata{i}.spontaneous(j),MCdata{i}.dur(j),MCdata{i}.interval(j));
    end
end
fclose(fileID);
loadpath = 'C:\Users\Bradley Barth\Box\Home Folder bbb16\Sync\School\grill_lab\projects\multi-electrode-array\data\processed-data';
load(fullfile(loadpath,'MC_2019_11_18.mat'));
score = zeros(size(MC));
score2 = zeros(size(MC));
count = zeros(length(MC),3);

formatPlot = 'Subject: %s\nDrug: %s\nReplicate: %s\nScore: %s\nScore2: %s';
formatPrint = '%i,%s,%i,%.3f,%i,%i,%i,%.3f\n';

diffRange = 0.02; 
% percent difference used to evaluate the width or robustness of the total
% number of events as a function of threshold (almost a slope evaluation on
% the green curve) independent of the threshold identified in the binary
% search method in the "contractDetectSingleThreshold.m" function
fileID = fopen(fullfile(loadpath,'mc-cdf-score.txt'),'w');
for i = 1:length(MC)
    yyaxis left
    semilogx(MC(i).cdf.Threshold,MC(i).cdf.Noise,'b-',...
        MC(i).cdf.Threshold,MC(i).cdf.Event,'r-',...
        MC(i).threshold*[1 1],[0 max(MC(i).cdf.Noise+MC(i).cdf.Event)],'k-',...
        MC(i).cdf.Threshold,MC(i).cdf.Noise+MC(i).cdf.Event,'g-')
        
    count(i,1) = sum(MC(i).data.time < 30*60);
    count(i,2) = sum((MC(i).data.time > 30*60).*(MC(i).data.time < 45*60));
    count(i,3) = sum(MC(i).data.time > 45*60);
    
    [~,I] = min(abs(MC(i).cdf.Threshold-MC(i).threshold/2));
    score(i) = MC(i).cdf.Event(I)/max(MC(i).cdf.Event);    
    
    total = MC(i).cdf.Noise+MC(i).cdf.Event;
    rangeScore = zeros(size(MC(i).cdf.Threshold)); probScore = zeros(size(rangeScore));
    [~,StartPoint] = max(total);
    for j = StartPoint:length(MC(i).cdf.Threshold)
        rangeScore(j) = j - find(abs(total(1:j)-total(j))>=diffRange,1,'last');
        probScore(j) = normpdf(total(j)*2,16.9,7.34);
    end
    rangeScore = rangeScore/max(rangeScore);
    probScore = probScore/max(probScore);
    combScore = rangeScore.*probScore;
    yyaxis right
    semilogx(MC(i).cdf.Threshold,rangeScore,'b--',...
        MC(i).cdf.Threshold,probScore,'r--',...
        MC(i).cdf.Threshold,combScore,'g--');
    score2(i) = probScore(I)*rangeScore(I);
    
    x = xlim; y = ylim;
    s1 = sprintf(formatPlot,num2str(MC(i).rat),MC(i).drug,...
        num2str(MC(i).day),num2str(round(score(i),2)),num2str(round(score2(i),10)));
    text(10^(log10(x(1))+0.5),(y(2)-y(1))*0.9+y(1),s1);
    pause()
    
%     fprintf(fileID,formatPrint,MC(i).rat,MC(i).drug,MC(i).day,MC(i).threshold,...
%         count(i,1),count(i,2),count(i,3),score(i));
end
fclose(fileID);
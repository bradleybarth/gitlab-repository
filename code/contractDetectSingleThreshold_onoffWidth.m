function [amp, width, peaktime, auc, start, stop] = contractDetectSingleThreshold_onoffWidth(EMG,Fs)

N = length(EMG);
time = (1:N)/Fs;

th = mean(EMG)+2*std(EMG);
filename = 'F:\Gen2 MEA\old_style_MC-threshold.txt';
fid = fopen(filename,'a'); 
fwrite(fid,[num2str(round(th,2)),'\n']);
fclose(fid);

bin = nan(size(EMG));
bin(EMG<th) = 0;
bin(EMG>=th) = 1;
bin(1:5) = 0; bin(end-5:end) = 0;

difbin = diff(bin);
onWidth = 5.5; onInd = onWidth * Fs;
offWidth = 3; offInd = offWidth * Fs;
minWidth = 0.01; minInd = minWidth *Fs;

sections = [0 find(abs(difbin)>0) length(bin)];
[~,I] = find(diff(sections)<minInd);
for i = 1:length(I)
    bin(sections(I(i))+1:sections(I(i)+1)) = 1;
end
difbin = diff(bin);
sections = [0 find(abs(difbin)>0) length(bin)];
i = 0;
if isempty(min(diff(sections)))
    disp('No MCs found')
    amp = nan; width = nan; peaktime = nan;
    auc = nan; start = nan; stop = nan;
    return
end
while min(diff(sections)) < offInd && i <= 9999
    [wid,I] = min(diff(sections));
    if bin(sections(I)+1) == 0&& wid < offInd
        bin(sections(I)+1:sections(I+1)) = not(bin(sections(I)+1:sections(I+1)));
    elseif bin(sections(I)+1) == 1 && wid < onInd
        bin(sections(I)+1:sections(I+1)) = not(bin(sections(I)+1:sections(I+1)));
    end
    difbin = diff(bin);
    sections = [0 find(abs(difbin)>0) length(bin)];
    i = i + 1;
end
if i > 9999
    disp('Maxed out while loop #1')
end
bin(1:5) = 0; bin(end-5:end) = 0;
difbin = diff(bin);
sections = [find((difbin)>0);find((difbin)<0)];
i = 0;
if isempty(min(diff(sections)))
    disp('No MCs found')
    amp = nan; width = nan; peaktime = nan;
    auc = nan; start = nan; stop = nan;
    return
end
while min(diff(sections)) < onInd && i <= 9999
    [~,I] = min(diff(sections));
    bin(sections(1,I)+1:sections(2,I)) = not(bin(sections(1,I)+1:sections(2,I)));
    difbin = diff(bin);
    sections = [find((difbin)>0);find((difbin)<0)];
    i = i+1;
    if isempty(min(diff(sections)))
        disp('No MCs found')
        amp = nan; width = nan; peaktime = nan;
        auc = nan; start = nan; stop = nan;
        return
    end
end
if i > 9999
    disp('Maxed out while loop #2')
end

startIdx = find(difbin>0);
stopIdx = find(difbin<0);

if isempty(stopIdx) || isempty(startIdx)
    amp = nan; width = nan; peaktime = nan;
    auc = nan; start = nan; stop = nan;
    return
end
start = time(startIdx(:));
stop = time(stopIdx(:));

width = stop-start;

C = length(width);

peaktime = nan(1,C);
auc = nan(1,C);
amp = nan(1,C);
for i = 1:C
    [~,I] = max(EMG(startIdx(i):stopIdx(i)));
    if isempty(I), continue, end
    peaktime(i) = time(I+startIdx(i));
    auc(i) = sum(EMG(startIdx(i):stopIdx(i)));
    amp(i) = auc(i)/width(i);
end